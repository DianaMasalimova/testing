
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Untitled {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
    driver = new FirefoxDriver();
    baseUrl = "http://stackoverflow.com";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testUntitled2() throws Exception {
    driver.get(baseUrl + "/");
    LogIn("DianaMasalimova96@gmail.com", "namogo05"); 
    ClickOnButton("week");
    ClickOnButton("Users");
    ClickOnButton("1");
    EditMyProfile();
    EditMyLocation("Kazan"); 
   
    
    
    
  }

private void EditMyLocation(String location) {
	WebElement element3 = driver.findElement(By.name("Location")); 
    element3.sendKeys(location);
    driver.findElement(By.xpath("(//input[@value='Save Profile']"));
}

private void EditMyProfile() {
	driver.findElement(By.linkText("Profile")).click();
    driver.findElement(By.linkText("click here to edit")).click();
}



private void ClickOnButton(String button) {
	driver.findElement(By.linkText(button)).click();
}

private void LogIn(String login, String password) {
	driver.findElement(By.xpath("(//a[contains(text(),'log in')])[2]")).click();   
    WebElement element = driver.findElement(By.name("email")); 
    element.sendKeys(login); 
    WebElement element1 = driver.findElement(By.name("password")); 
    element1.sendKeys(password); 
    WebElement element2 = driver.findElement(By.id("submit-button")); 
    element2.click();
}

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}

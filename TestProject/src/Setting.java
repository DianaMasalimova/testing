import java.io.File;

import java.io.IOException;
import java.lang.Object;
import javax.swing.text.Document;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.*;
import org.testng.internal.Graph.Node;
import org.xml.*;
import org.xml.sax.SAXException;

public class Setting {

	public static String file = "SettingXML.xml"; 
	private static String baseURL; 
	public static String login; 
	public static String password; 
	private static String user; 
	private static XmlElement document;
	public static DocumentBuilderFactory dbf;
	public static DocumentBuilder db;
	public static Document doc;
	
	
	
	 public  Setting() 
	 	{ 
		 if (!System.io.File.Exists(file)) 
		 { 
			 throw new Exception("Problem"); 
			 document = new XMLElement(); 
		 } 
	 	} 

	 public String getBaseURL()
	 	{  
		 if (baseURL == null) 
		 { 
			 NodeList node = doc.getElementsByTagName("baseURL"); 
			 baseURL = node.InnerText; 
		 } 
		 return baseURL;

	 	}  


	 public  String getLogin () 
	 	{ 

		 if (login == null) 
		 { 
			 NodeList node = doc.getElementsByTagName("login");
			 login = node.InnerText; 
		 } 
		 return login; 

	 	} 
	 
	 public String getPassword ()
	 	{ 

		 if (password == null) 
		 { 
			 NodeList node = doc.getElementsByTagName("password");
			 password = node.InnerText; 
		 } 
		 return password; 

	 	} 
	 public String getUser()  
	    {
		 if (user == null) 
		 { 
			 NodeList node = doc.getElementsByTagName("user"); 
			 user = node.InnerText; 
		 } 
		 return user;  
		} 
	
}



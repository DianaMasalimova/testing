import org.junit.Assert;
import org.junit.Test;

public class EditTagTest extends TestBase {
	
	
	@Test
	public void EditTag(){
		app.navigationHelper.GoToMainPage();
		app.tegHelper.GoEditTag();
		TegData teg = new TegData("testing");
		app.tegHelper.FillTegForm(teg);
		Assert.assertTrue((boolean) app.loginHelper.IsLoggedIn());
	}

}

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

public class LoginTest extends TestBase {

	
	public void LoginWithValidCreditionals(){
		AccountData userdata = new AccountData("DianaMasalimova96@gmail.com", "namogo05");
		app.loginHelper.LogIn(userdata);
		app.loginHelper.Logout();
		Assert.assertTrue((boolean) app.loginHelper.IsLoggedIn());
	}

}

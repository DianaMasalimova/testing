import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

public class ProfileHelper extends HelperBase{

	public ProfileHelper(AppManager manager) {
		super(manager);
		// TODO Auto-generated constructor stub
	}
	
	public void ClickForEdit(){
		PressButton("click here to edit");
	}



	public void FillLocationForm(ProfileData location) {
		FillData("locations", location.City);
		SaveProfile();
		// TODO Auto-generated method stub
		
	}

	
	private void SaveProfile() {
		// TODO Auto-generated method stub
		 driver.findElement(By.xpath("(//input[@value='Save Profile']")).click();
	}

	
}

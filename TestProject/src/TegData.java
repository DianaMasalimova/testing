
public class TegData {
	
	public String Teg;

	public TegData(String teg) {
		Teg = teg;
	}

	public String getTeg() {
		return Teg;
	}

	public void setTeg(String teg) {
		Teg = teg;
	}

}

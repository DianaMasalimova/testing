import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;


	public class AppManager{
		private String  baseUrl;
		protected WebDriver driver;
		protected LoginHelper loginHelper;
		protected NavigationHelper navigationHelper;
		protected ProfileHelper profileHelper;
		protected TegHelper tegHelper;
		
	
		private static ThreadLocal<AppManager> app = new ThreadLocal<>();
		
		public ProfileHelper getProfileHelper() {
			return profileHelper;
		}
		
		
		

		public TegHelper getTegHelper() {
			return tegHelper;
		}


		public AppManager(){
			driver = new FirefoxDriver();
			baseUrl = "http://stackoverflow.com";
			loginHelper = new LoginHelper(this);
			navigationHelper = new NavigationHelper();
			profileHelper = new	ProfileHelper(this);
			tegHelper = new TegHelper(this);
			
		}
		
		private static AppManager GetInstance(){
			if(!app.equals(null)){
				AppManager newInstance = new AppManager();
				newInstance.navigationHelper.GoToHomePage();
				app.set(newInstance);
			}
			return app.get();
		}
	
		
		
		 public WebDriver getWebDriver() {
		        return driver;
		       }
		    	
		 public LoginHelper getLoginHelper() {
		        return loginHelper;
		       }
		 public NavigationHelper getNavigationHelper() {
		        return navigationHelper;
		       }
		   
		public void Stop(){
			try
			{
				driver.quit();
			}
			catch(Exception e)
			{
				//Ignore errors if unable to close  the  browser
			}
		}
	}

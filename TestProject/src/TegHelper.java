import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class TegHelper extends HelperBase {

	public TegHelper(AppManager manager) {
		super(manager);
		// TODO Auto-generated constructor stub
	}
	
	public void GoEditTag(){
		PressButton("edit");
	}

	public void FillTegForm(TegData teg) {
		FillData("interestingTag",teg.Teg);
		WebElement element = driver.findElement(By.id("interestingAdd")); 
		element.click();
	}

}

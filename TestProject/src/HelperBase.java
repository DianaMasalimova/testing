import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class HelperBase {
	
	protected WebDriver driver;
	protected static AppManager manager;
	public boolean acceptNextAlert = true;
	private StringBuffer verificationErrors = new StringBuffer();
	
	public HelperBase (AppManager manager) {
		this.manager = manager;
		driver = manager.driver;
	}
	
	public void FillData (String value, String type){
   	    WebElement element = driver.findElement(By.id(type)); 
		element.sendKeys(toString());
    }
    public void PressButton (String value){
   	    driver.findElement(By.linkText(value)).click();
    }
	
    
    private boolean isElementPresent(By by) {
	    try {
	      driver.findElement(by);
	      return true;
	    } catch (NoSuchElementException e) {
	      return false;
	    }
	}
    
    private boolean isAlertPresent() {
	    try {
	      driver.switchTo().alert();
	      return true;
	    } catch (NoAlertPresentException e) {
	      return false;
	    }
   }

	private String closeAlertAndGetItsText() {
	    try {
	      Alert alert = driver.switchTo().alert();
	      String alertText = alert.getText();
	      if (acceptNextAlert) {
	        alert.accept();
	      } else {
	        alert.dismiss();
	      }
	      return alertText;
	    } finally {
	      acceptNextAlert = true;
	    }
	}
	
	
	
	  

}


import org.junit.After;
import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

public class TestBase{
	
		protected AppManager app;
		public StringBuilder verificationErrors;
		
		//loginTest
		public void setUpTest(){
			app = new AppManager();
			verificationErrors = new StringBuilder();
			app.driver.manage().window().maximize();
			app.navigationHelper.GoToHomePage();			
			app.loginHelper.LogIn(new AccountData("DianaMasalimova96@gmail.com", "namogo05"));
			Assert.assertTrue((boolean) app.loginHelper.IsLoggedIn());
		}

		  @After
	      public void tearDown() throws Exception {
			    RemoteWebDriver driver = null;
				driver.quit();
			    Object verificationErrors = null;
				String verificationErrorString = verificationErrors.toString();
			    if (!"".equals(verificationErrorString)) {
			      fail(verificationErrorString);
			    }
		  }

		private void fail(String verificationErrorString) {
			// TODO Auto-generated method stub
			
		}
}



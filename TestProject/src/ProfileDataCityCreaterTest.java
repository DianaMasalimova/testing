import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

public class ProfileDataCityCreaterTest extends TestBase{
	
	@Test
	public void EditCityTest(){
		app.navigationHelper.GoToProfile();
		app.profileHelper.ClickForEdit();
		ProfileData location = new ProfileData("Kazan");
		app.profileHelper.FillLocationForm(location);
		Assert.assertTrue((boolean) app.loginHelper.IsLoggedIn());
	}

	
	

}
